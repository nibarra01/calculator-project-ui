import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalcComponent } from './cmp/calc/calc.component';
import { HttpClientModule } from '@angular/common/http';
// import { AddComponent } from './cmp/func/add/add.component';
// import { SubComponent } from './cmp/func/sub/sub.component';
// import { MulComponent } from './cmp/func/mul/mul.component';
// import { DivComponent } from './cmp/func/div/div.component';
// import { PowComponent } from './cmp/func/pow/pow.component';
// import { PerComponent } from './cmp/func/per/per.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    CalcComponent,
    // AddComponent,
    // SubComponent,
    // MulComponent,
    // DivComponent,
    // PowComponent,
    // PerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
