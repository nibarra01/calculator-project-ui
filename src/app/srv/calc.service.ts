import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { resolveTypeReferenceDirective } from 'typescript';

@Injectable({
  providedIn: 'root'
})
export class CalcService {
  var: any = null;
  observeable = new BehaviorSubject<any>(this.var);


  constructor(private http: HttpClient) {  }

  rtn(): Observable<any> {
    return this.observeable.asObservable();
  }

  add(n1: number, n2: number){
    // console.log("d " + this.var);
    this.http.get(`${environment.url}/add?x=${n1}&y=${n2}`)
    .toPromise().then((calced) => {
      this.var = calced;
      // console.log("a " + this.var);
      this.observeable.next(this.var);
    });

    // console.log("c " + this.var);
  }

  sub(n1: number, n2: number) {
    this.http.get(`${environment.url}/sub?x=${n1}&y=${n2}`)
    .toPromise().then((calced) => {
      this.var = calced;
      this.observeable.next(this.var);
    });
  }
  mul(n1: number, n2: number) {
    this.http.get(`${environment.url}/mul?x=${n1}&y=${n2}`)
    .toPromise().then((calced) => {
      this.var = calced;
      this.observeable.next(this.var);
    });
  }
  div(n1: number, n2: number) {
    this.http.get(`${environment.url}/div?x=${n1}&y=${n2}`, {responseType: 'text'})
    .toPromise().then((calced) => {
      this.var = calced;
      this.observeable.next(this.var);
    });
  }
  mod(n1: number, n2: number) {
    this.http.get(`${environment.url}/mod?x=${n1}&y=${n2}`)
    .toPromise().then((calced) => {
      this.var = calced;
      this.observeable.next(this.var);
    });
  }
  pow(n1: number, n2: number) {
    this.http.get(`${environment.url}/pow?x=${n1}&y=${n2}`)
    .toPromise().then((calced) => {
      this.var = calced;
      this.observeable.next(this.var);
    });
  }
  per(n1: number, n2: number) {
    this.http.get(`${environment.url}/per?x=${n1}&y=${n2}`, {responseType: 'text'})
    .toPromise().then((calced) => {
      this.var = calced;
      this.observeable.next(this.var);
    });
  }

  clear(): void {
    this.observeable.next(null);
  }
}
