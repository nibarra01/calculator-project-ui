import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CalcService } from 'src/app/srv/calc.service';

@Component({
  selector: 'app-calc',
  templateUrl: './calc.component.html',
  styleUrls: ['./calc.component.css']
})
export class CalcComponent implements OnInit {
  rtn: any = null;
  mathType = 'none';
  moredots = false;

  formy = new FormGroup({
    num1: new FormControl(''),
    num2: new FormControl('')
  });

in1: number = null;
in2: number = null;
polarity1 = true;
polarity2 = true;
end: string;


  constructor(private backMath: CalcService) { }

  ngOnInit(): void {
    this.backMath.rtn().subscribe(R => this.rtn = R);
   }


  func(a: number, b: number): void{
    // console.log(a);
    // console.log(b);


    switch (this.mathType){
      case 'add':
        this.backMath.add(a, b);
        break;
      case 'sub':
        this.backMath.sub(a, b);
        break;
      case 'mul':
        this.backMath.mul(a, b);
        break;
      case 'div':
        this.backMath.div(a, b);
        break;
      case 'pow':
        this.backMath.pow(a, b);
        break;
      case 'per':
        this.backMath.per(a, b);
        break;
      default:
        console.log('An error has occured');
    }
    // console.log( "b " + this.rtn);
  }

  update1(): void{
    this.in1 = this.formy.get('num1').value;
    const check: string = this.in1.toString();
    this.moredots = true;
    if (check.indexOf('.') > -1){
      this.moredots = false;
    }
    if (check.length < 1) {
      this.moredots = false;
    }
    this.backMath.clear();
  }
  update2(): void{
    this.in2 = this.formy.get('num2').value;
    const in2string = this.in2.toString();
    const last = in2string.substr(in2string.length - 1);
    switch (last){
      case '1':
        this.end = 'st';
        break;
      case '2':
        this.end = 'nd';
        break;
      case '3':
        this.end = 'rd';
        break;
      case '.':
        this.end = '';
        break;
      default:
        this.end = 'th';
        break;
    }
    if (in2string.substr(in2string.length - 2) === '11' ||
    in2string.substr(in2string.length - 2) === '12' ||
    in2string.substr(in2string.length - 2) === '13' ){
      this.end = 'th';
    }

    this.moredots = true;
    if (in2string.indexOf('.') > -1){
      this.moredots = false;
    }
    if (in2string.length < 1){
      this.moredots = false;
    }
    this.backMath.clear();
  }


  onlyNums(e): boolean{
    const charCode = (e.which) ? e.which : e.keycode;
    if (charCode === 46 && this.moredots === true){
      return true;
    } else if (charCode > 31 && ((charCode < 48 || charCode > 57))){
      return false;
    }
    return true;
  }

  setAdd(): void{
    this.backMath.clear();
    this.mathType = 'add';
    this.in1 = null;
    this.in2 = null;
    this.formy.reset();
    this.polarity1 = true;
    this.polarity2 = true;
  }

  setSub(): void{
    this.backMath.clear();
    this.mathType = 'sub';
    this.in1 = null;
    this.in2 = null;
    this.formy.reset();
    this.polarity1 = true;
    this.polarity2 = true;
  }

  setMul(): void{
    this.backMath.clear();
    this.mathType = 'mul';
    this.in1 = null;
    this.in2 = null;
    this.formy.reset();
    this.polarity1 = true;
    this.polarity2 = true;
  }

  setDiv(): void{
    this.backMath.clear();
    this.mathType = 'div';
    this.in1 = null;
    this.in2 = null;
    this.formy.reset();
    this.polarity1 = true;
    this.polarity2 = true;
  }

  setPow(): void{
    this.backMath.clear();
    this.mathType = 'pow';
    this.in1 = null;
    this.in2 = null;
    this.formy.reset();
    this.polarity1 = true;
    this.polarity2 = true;
  }

  setPer(): void{
    this.backMath.clear();
    this.mathType = 'per';
    this.polarity1 = true;
    this.polarity2 = true;
    this.in1 = null;
    this.in2 = null;
    this.formy.reset();
  }

  flip1(): void{
    this.polarity1 = !this.polarity1;
    this.in1 = this.in1 * -1;
    this.backMath.clear();
  }
  flip2(): void{
    this.polarity2 = !this.polarity2;
    this.in2 = this.in2 * -1;
    this.backMath.clear();
  }
}
