import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalcComponent } from './cmp/calc/calc.component';
// import { AddComponent } from './cmp/func/add/add.component';
// import { DivComponent } from './cmp/func/div/div.component';
// import { MulComponent } from './cmp/func/mul/mul.component';
// import { PerComponent } from './cmp/func/per/per.component';
// import { PowComponent } from './cmp/func/pow/pow.component';
// import { SubComponent } from './cmp/func/sub/sub.component';

const routes: Routes = [
  {
    path: '',
    component: CalcComponent
  },
  // {
  //   path: 'add',
  //   component: AddComponent
  // },
  // {
  //   path: 'sub',
  //   component: SubComponent
  // },
  // {
  //   path: 'mul',
  //   component: MulComponent
  // },
  // {
  //   path: 'div',
  //   component: DivComponent
  // },
  // {
  //   path: 'pow',
  //   component: PowComponent
  // },
  // {
  //   path: 'per',
  //   component: PerComponent
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
